FROM nginx:alpine
ARG flow

RUN apk update && apk add \
        mc \
        tmux 

COPY index-$flow.html /usr/share/nginx/html/index.html
